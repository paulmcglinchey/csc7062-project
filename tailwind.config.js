const { lightBlue } = require('tailwindcss/colors');
const colors = require('tailwindcss/colors');

module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      zIndex: {
        '-1': -1
      },
      maxWidth: {
        '3/5': '60%'
      },
      colors: {
        'springgreen': 'springgreen',
        'hotpink': 'hotpink',
        rose: colors.rose,
        orange: colors.orange,
        emerald: colors.emerald,
        lime: colors.lime,
        cyan: colors.cyan,
        lightBlue: colors.lightBlue
      }
    },
  },
  variants: {
    extend: {
      animation: ['hover', 'focus'],
      display: ['hover', 'group-hover'],
      visibility: ['hover', 'group-hover']
    },
  },
  plugins: [],
}
