<?php

require('../gaeaConnect.php');
require('../joinVehicleTablesSingle.php');
$connection = connectToGaea();

// no connection to the database
if (!$connection) {
  $output['error'] = $connection -> error;
  echo json_encode($output);
  exit;
}

$horizontal = $connection -> real_escape_string($_GET['hor']);
$num_selections = $connection -> real_escape_string($_GET['verts']);
$selections = array();
$query = "";

for ($x = 0; $x < $num_selections; $x++) {
  $selections[] = $connection -> real_escape_string($_GET['selection'.$x]);
}

// get all the label values first
$labelQuery = $connection -> query(joinVehicleTablesAdditive($connection, $horizontal, "ORDER BY $horizontal ASC"));
$labels = array();

if ($labelQuery -> num_rows > 0) {
  while ($row = ($labelQuery) -> fetch_array(MYSQLI_NUM)) {
    $labels[] = (integer) $row[0];
  }
}

$output['labels'] = $labels;

$datasets = array();

foreach ($selections as $selection) {
  
  $selectionData = array();
  $selectionData['name'] = $selection;
  
  foreach ($labels as $label) {
    $query = ($connection -> query(joinVehicleTablesAdditive($connection, "AVG($selection) AS average", "WHERE $horizontal='$label'"))) -> fetch_array(MYSQLI_NUM);
    $selectionData['data'][] = (float) $query[0];
  }

  $datasets[] = $selectionData;
}

$output['datasets'] = $datasets;

echo json_encode($output);
exit;

?>