<?php

require('../gaeaConnect.php');
require('../joinVehicleTablesSingle.php');
$connection = connectToGaea();

// no connection to the database
if (!$connection) {
  $output['error'] = $connection -> error;
  echo json_encode($output);
  exit;
}

$query = $connection -> real_escape_string($_GET['query']);

// get all the label values first
$labelQuery = $connection -> query(joinVehicleTablesAdditive($connection, $query, ""));
$labels = array();

if ($labelQuery -> num_rows > 0) {
  while ($row = ($labelQuery) -> fetch_array(MYSQLI_NUM)) {
    $labels[] = $row[0];
  }
}

$output['labels'] = $labels;

$dataset = array();

foreach ($labels as $label) {
  $sql = ($connection -> query(joinVehicleTablesAdditive($connection, "COUNT(*)", "WHERE $query='$label'"))) -> fetch_array(MYSQLI_NUM);
  $dataset[] = (integer) $sql[0];
}

$output['data'] = $dataset;

echo json_encode($output);
exit;

?>