<?php

require('../gaeaConnect.php');
$connection = connectToGaea();

// no connection to the database
if (!$connection) {
  $output['error'] = $connection -> error;
  echo json_encode($output);
  exit;
}

if (!isset($_GET['needImages']) || !isset($_GET['typeContent'])) {
  http_response_code(400);
  $output['error'] = "API needs correct parameters to fulfill the request";
  echo json_encode($output);
  exit;
}

$needImages = $connection -> real_escape_string($_GET['needImages']);
$typeContent = $connection -> real_escape_string($_GET['typeContent']);

$sql = "";
$idField = $typeContent === "gaea_about" ? "AboutID" : "LandingID";

if ($needImages) {
  $sql = "SELECT $idField, Title, Summary, URL FROM $typeContent 
          INNER JOIN gaea_images ON $typeContent.ImageID = gaea_images.ImageID
          INNER JOIN gaea_admins ON $typeContent.AdminID = gaea_admins.AdminID;";
} else {
  $sql = "SELECT $idField, Title, Summary FROM $typeContent;";
}


// make the query and save to result
$result = $connection -> query($sql);

// empty array to store the output data
$data = array();

if ($result) {
  while ($row = $result -> fetch_assoc()) {
    if ($needImages) {
      $data[] = ['id' => $row[$idField], 'title' => $row['Title'], 'summary' => $row['Summary'], 'url' => $row['URL']];
    } else {
      $data[] = ['id' => $row[$idField], 'title' => $row['Title'], 'summary' => $row['Summary']];
    }
  }
} else {
  $output['message'] = "0 rows returned from that query";
}

$output['data'] = $data;
echo json_encode($output);


exit;

?>