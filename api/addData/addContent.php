<?php

$output = array();

require('../gaeaConnect.php');
require('../logging/logitem.php');
require('./addImage.php');

$connection = connectToGaea();

if ($connection -> error) {
  $output['error'] = $connection -> error;
  exit;
}

// Takes raw data from the request
$json = file_get_contents('php://input');

// Converts it into a PHP object
$data = json_decode($json, true);

if (!isset($data['typeContent']) || !isset($data['title']) || !isset($data['summary']) || !isset($data['content']) || !isset($data['username'])) {
  $output['error'] = "an input didn't make it to the API";
  echo json_encode($output);
  http_response_code(400);
  exit;
}


// get data from post
$typeContent = $connection -> real_escape_string($data['typeContent']);
$title = $connection -> real_escape_string($data['title']);
$summary = $connection -> real_escape_string($data['summary']);
$content = $connection -> real_escape_string($data['content']);
$username = $connection -> real_escape_string($data['username']);

// declare an insert statement empty
$sql = "";

// check if an image url was sent
if (isset($data['image'])) {
  $image = $data['image'];

  // add the image to the database using secondary script
  if (addImage($connection, $image, $username) === FALSE) {
    http_response_code(400);
    $output['error'] = "Image could not be added";
    echo json_encode($output);
    exit;
  }

  // image based sql insert statement
  $sql = "INSERT INTO $typeContent (Title, Summary, Content, ImageID, AdminID) VALUES
          ('$title', '$summary', '$content',
          (SELECT ImageID from gaea_images WHERE URL='$image'),
          (SELECT AdminID from gaea_admins WHERE username='$username'));";
} else {
  // alternative route with no image
  $sql = "INSERT INTO $typeContent (Title, Summary, Content, AdminID) VALUES
          ('$title', '$summary', '$content',
          (SELECT AdminID from gaea_admins WHERE username='$username'));";
}

if ($connection -> query($sql)) {
  // log the entry
  logItem($connection, $username, "added", "article ".$connection -> insert_id);
  http_response_code(200);
  $output['message'] = "success!";
} else {
  $output['error'] = $connection -> error; 
}

echo json_encode($output);

exit;

?>