<?php

function addImage($connection, $image, $username) {

  $image = $connection -> real_escape_string($image);

  // check if the image already exists in the database
  $result = $connection -> query("SELECT * from gaea_images WHERE URL='$image';");
  if ($result -> num_rows > 0) {
    return TRUE;
  } else {
    // if not add it
    $sql = "INSERT INTO gaea_images (URL) VALUES ('$image');";
  
    if ($connection -> query($sql)) {
      logItem($connection, $username, "added", "image ".$connection -> insert_id);
      return TRUE;
    } else {
      return FALSE;
    }
  }

}

?>