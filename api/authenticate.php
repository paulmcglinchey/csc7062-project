<?php
// assoc array to store the return info
$output = array();

include('gaeaConnect.php');
include('./logging/logitem.php');
$connection = connectToGaea();

// Takes raw data from the request
$json = file_get_contents('php://input');

// Converts it into a PHP object
$data = json_decode($json, true);

if (!isset($data['username']) || !isset($data['password'])) {
  http_response_code(400);
  exit;
}

$username = $data['username'];
$password = hash("sha256", $data['password'], false);

$results = $connection -> query("SELECT * FROM gaea_admins WHERE username='$username' AND password='$password'");
while ($row = $results->fetch_object()) {
  $results_arr = $row;
}

// add user to logged in table if login was successful
// current date
if ($results_arr) {
  
  // log activity
  logItem($connection, $username, "logged in", NULL);

  $output['isUserAuthenticated'] = true;
  $output['username'] = $username;
  http_response_code(200);
} else {
  http_response_code(400);
  $output['isUserAuthenticated'] = false;
}

echo json_encode($output);

exit;
?>