<?php

require('./gaeaConnect.php');
require('./joinVehicleTables.php');
$connection = connectToGaea();

// no connection to the database
if (!$connection) {
  $output['error'] = $connection -> error;
  echo json_encode($output);
  exit;
}

$selection1 = $connection -> real_escape_string($_GET['selection1']);
$selection2 = $connection -> real_escape_string($_GET['selection2']);

// bad request
if (!isset($selection1) || !isset($selection2)) {
  http_response_code(400);
  exit;
}

// make a request for some data
$sql = joinVehicleTables($connection, $selection1, $selection2);

$result = $connection -> query($sql);

$data = array();

if ($result -> num_rows > 0) {
  while ($row = $result -> fetch_assoc()) {
    $data[] = ['x' => (float) $row[$selection1], 'y' => (float) $row[$selection2]];
  }
} else {
  $data = "0 rows returned";
}

if ($data) {
  $output['data'] = $data;
} else {
  http_response_code(400);
  exit;
}

echo json_encode($output);

?>