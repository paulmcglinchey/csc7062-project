import NavHeader from './components/NavHeader.js';
import Landing from './components/Landing.js';
import About from './components/About.js';
import Analytics from './components/Analytics.js';
import Admin from './components/Admin.js';
import LoginModal from './components/LoginModal.js';
import AboutArticle from './components/AboutArticle.js';
import LandingArticle from './components/LandingArticle.js';
import { Fragment, useState } from 'react';
import {
  HashRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

const App = () => {

  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
  const toggleLoginModal = () => setIsLoginModalOpen(!isLoginModalOpen);

  const [isUserAuthenticated, setIsUserAuthenticated] = useState(false);
  const [loggedInUser, setLoggedInUser] = useState();

  const [isHeroBeingUsed, setIsHeroBeingUsed] = useState(true);

  if (isLoginModalOpen) {
    document.body.classList.add("isModalOpen");
  } else if (!isLoginModalOpen && document.body.classList.contains("isModalOpen")) {
    document.body.classList.remove("isModalOpen");
  }

  return (
    <Fragment>
      <Router>
        <NavHeader isHeroBeingUsed={isHeroBeingUsed} toggleLoginModal={toggleLoginModal} isLoginModalOpen={isLoginModalOpen} isUserAuthenticated={isUserAuthenticated} />
        <Switch>
          <Route path="/about">
            <About setIsHeroBeingUsed={setIsHeroBeingUsed} />
          </Route>
          <Route path="/analytics">
            <Analytics setIsHeroBeingUsed={setIsHeroBeingUsed} />
          </Route>
          <Route path="/aboutArticle/:id">
            <AboutArticle setIsHeroBeingUsed={setIsHeroBeingUsed} />
          </Route>
          <Route path="/landingArticle/:id">
            <LandingArticle setIsHeroBeingUsed={setIsHeroBeingUsed} />
          </Route>
          <Route path="/admin">
            <Admin isUserAuthenticated={isUserAuthenticated} setIsHeroBeingUsed={setIsHeroBeingUsed} loggedInUser={loggedInUser} setLoggedInUser={setLoggedInUser} setIsUserAuthenticated={setIsUserAuthenticated} />
          </Route>
          <Route path="/">
            <Landing setIsHeroBeingUsed={setIsHeroBeingUsed} />
          </Route>
        </Switch>
        <LoginModal isUserAuth={isUserAuthenticated} setUserAuth={setIsUserAuthenticated} toggleLoginModal={toggleLoginModal} isOpen={isLoginModalOpen} setLoggedInUser={setLoggedInUser} />
      </Router>
    </Fragment>
  );

}

export default App;
