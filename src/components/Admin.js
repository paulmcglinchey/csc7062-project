import { Fragment } from 'react';
import { Redirect } from 'react-router-dom';
import Content from './Content.js';
import TickMarkSVG from './svg/TickMarkSVG.js';
import DataEntryOption from './DataEntryOption.js';
import options from '../json/dataEntryOptions.json';
import DashboardContainer from './DashboardContainer.js';
import AddUser from './AddUser.js';
import CSVUploader from './utilities/CSVUploader.js';
import AddAnalytic from './AddAnalytics.js';
import AddContent from './AddContent.js';
import WIP from './WIP.js';
import ActivityLog from './ActivityLog.js';
import RemoveContent from './RemoveContent.js';

const Admin = (props) => {

  const logOutUser = () => {
    props.setIsUserAuthenticated(false);
    props.setLoggedInUser(null);
  }

  if (props.isUserAuthenticated) {
    return (
      <Fragment>
        <Content needsHero={false} setIsHeroBeingUsed={props.setIsHeroBeingUsed}>
          <div className="w-full mt-5">
            <div className="rounded">
              <div className="flex flex-col sm:flex-row flex-wrap justify-between mb-3 space-y-2 sm:space-y-0">
                <div className="flex justify-center md:justify-none">
                  <h1 className="text-3xl font-bold">Admin Dashboard</h1>
                </div>
                <button onClick={() => logOutUser()} className="flex justify-center items-center text-springgreen p-2 rounded-xl bg-black hover:text-white hover:bg-red-600 transition-all duration-600">
                  <div>
                    Logged in as {props.loggedInUser}
                  </div>
                  <div className="w-4 h-4 mx-1">
                    <TickMarkSVG />
                  </div>
                </button>
              </div>
              <div className="rounded">
                <DashboardContainer title="Activity" >
                  <ActivityLog />
                </DashboardContainer>
                <DashboardContainer title="Add Users">
                  <AddUser loggedInUser={props.loggedInUser} />
                </DashboardContainer>
                <DashboardContainer title="Add Analytics">
                  <AddAnalytic />
                </DashboardContainer>
                <DashboardContainer title="Add Content">
                  <AddContent loggedInUser={props.loggedInUser} />
                </DashboardContainer>
                <DashboardContainer title="Remove Entries">
                  <RemoveContent loggedInUser={props.loggedInUser} />
                </DashboardContainer>
                <DashboardContainer title="Add data">
                  <div className="flex flex-col">
                    <DataEntryOption title={options[0].title} data={options[0].options}>
                      <CSVUploader loggedInUser={props.loggedInUser} />
                    </DataEntryOption>
                    <DataEntryOption title={options[1].title} data={options[1].options}>
                      <WIP />
                    </DataEntryOption>
                  </div>
                </DashboardContainer>
              </div>
            </div>
          </div>
        </Content>
      </Fragment >
    )
  } else {
    return (
      <Redirect exact to="/" />
    )
  }
}

export default Admin;