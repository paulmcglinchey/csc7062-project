import { useEffect, useState } from "react";
import endpoints from "../endpoints";
import DataSelector from "./utilities/DataSelector";
import PolarChart from '../charts/PolarChart.js';
import AdminSubmitButton from "./utilities/AdminSubmitButton";
import BarChart from "../charts/BarChart";
import DoughnutChart from "../charts/DoughnutChart";
import PieChart from "../charts/PieChart";

const AddCategorical = (props) => {

  const [selection, setSelection] = useState('FuelType');
  const [chartType, setChartType] = useState('polar');

  const [labels, setLabels] = useState(null);
  const [data, setData] = useState(null);

  const dataOptions = [
    { 'name': 'Fuel Type', 'query': 'FuelType' },
    { 'name': 'Manufacturer', 'query': 'Manufacturer' },
    { 'name': 'Transmission', 'query': 'Transmission' },
    { 'name': 'Tax Band', 'query': 'TaxBand' },
    { 'name': 'Euro Standard', 'query': 'EuroStandard' }
  ]

  const chartOptions = [
    'bar', 'polar', 'pie', 'doughnut'
  ]

  const onSubmit = () => {
    fetch(`${endpoints.getCategoricalCounts}?query=${selection}`, {
      method: 'GET'
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          setLabels(result.labels);
          setData(result.data);
        }
      )
      .catch(
        (error) => {
          console.log(error);
        }
      )
  }

  useEffect(() => {
    fetch(`${endpoints.getCategoricalCounts}?query=${selection}`, {
      method: 'GET'
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          setLabels(result.labels);
          setData(result.data);
        }
      )
      .catch(
        (error) => {
          console.log(error);
        }
      )
  }, [])

  return (
    <div className="flex flex-col w-auto">
      <form onSubmit={(e) => {
        e.preventDefault();
        onSubmit();
      }}>
        <div className="flex flex-col flex-wrap md:flex-row gap-1 justify-between">
          <DataSelector onChange={setSelection} value={selection} label="Data selection">
            {dataOptions.map((option, key) => {
              return (
                <option value={option.query} key={key}>{option.name}</option>
              )
            })}
          </DataSelector>
          <DataSelector onChange={setChartType} value={chartType} label="Chart selection">
            {chartOptions.map((option, key) => {
              return (
                <option value={option} key={key}>{option}</option>
              )
            })}
          </DataSelector>
          <AdminSubmitButton title="Submit" />
        </div>
      </form>
      {data &&
        <div>
          {chartType === 'polar' &&
            <PolarChart data={data} labels={labels} label={selection} />
          }
          {chartType === 'bar' &&
            <BarChart data={data} labels={labels} label={selection} />
          }
          {chartType === 'doughnut' &&
            <DoughnutChart data={data} labels={labels} label={selection} />
          }
          {chartType === 'pie' &&
            <PieChart data={data} labels={labels} label={selection} />
          }
        </div>
      }
    </div>
  )
}

export default AddCategorical;