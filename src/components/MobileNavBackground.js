const MobileNavBackground = (props) => {
    if (props.isOpen) {

        const mediaQuery = window.matchMedia('(min-width: 768px)');

        const handleTabletChange = (e) => {
            if (!e.matches) {
                document.body.classList.add("isMobileNavOpen");
            }
        }

        mediaQuery.addEventListener("resize", handleTabletChange);

        handleTabletChange(mediaQuery);
        

        return (
            <div className={`${props.isOpen ? "bg-opacity-100 block" : "bg-opacity-0 hidden"} absolute md:hidden bg-black w-screen h-screen z-10 inset-0 transition-all duration-700`} />
        )
    } else {
        if (!props.isOpen && document.body.classList.contains("isMobileNavOpen")) {
            document.body.classList.remove("isMobileNavOpen");
        }
        return null;
    }
}

export default MobileNavBackground;