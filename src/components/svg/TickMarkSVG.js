const TickMarkSVG = (props) => {
  return (
    <svg viewBox="0 0 48 48" className="stroke-current">
      <path
        stroke-width="8"
        fill="none"
        stroke-linecap="square"
        d="M 6.034702,28.671421 17.353061,39.98978 43.996405,13.346436" />
    </svg >
  )
}

export default TickMarkSVG;