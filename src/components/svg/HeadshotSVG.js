const HeadshotSVG = (props) => {
  return (
    <svg viewBox="0 0 10 10" className={`h-10 w-10 md:h-8 md:w-8`}>
      <path
        fill="none"
        className="stroke-current sm:group-hover:text-springgreen"
        d="M 6.5217586,2.7193854 A 1.5217586,1.3683567 0 0 1 5,4.0877421 1.5217586,1.3683567 0 0 1 3.4782414,2.7193854 1.5217586,1.3683567 0 0 1 5,1.3510287 1.5217586,1.3683567 0 0 1 6.5217586,2.7193854 Z M 2.1297487,9.1796875 V 7.4281517 c 0,-2.7860004 0,-2.7860004 2.8702512,-2.7860004 2.8702514,0 2.8702514,0 2.8702514,2.7860004 v 1.7515358"
      />
    </svg>
  )
}

export default HeadshotSVG;