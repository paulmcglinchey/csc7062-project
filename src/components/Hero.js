import React, { Fragment } from 'react';
import ChevronSVG from './svg/ChevronSVG.js';
import { Link } from 'react-router-dom';

const HeroVideo = (props) => {
    return (
        <Fragment>
            <div className="absolute top-0 left-0 right-0 -z-1 w-screen overflow-hidden hero-holder md:hero-holder-md" >
                <video className="relative md:min-w-full md:min-h-full md:object-cover max-w-none h-full -z-1" muted autoPlay playsInline loop src={props.source} />
                <div className="container md:mx-auto relative -top-2/3 px-5">
                    <div className="container xl:px-0 md:px-5">
                        <h1 className="max-w-screen-sm md:max-w-screen-md md:text-7xl sm:text-6xl text-3xl subpixel-antialiased font-bold">Painting a clearer picture of the past.</h1>
                        <button className="relative flex items-center hover:text-springgreen hover:bg-black hover:scale-105 transform transition-all duration-300 mt-4 rounded-full px-2 -left-2 z-50 group">
                            <Link to="/about"><span className="text-lg flex font-bold">Find out more</span></Link>
                            <span className="flex ml-2">
                                <ChevronSVG width="w-4" height="h-4" />
                            </span>
                        </button>
                    </div>
                </div>
            </div >
        </Fragment>
    )
};

export default HeroVideo;