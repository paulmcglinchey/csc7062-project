import { Fragment } from 'react';
import Hero from './Hero.js';

const heroVideo = "https://res.cloudinary.com/pmcglinchey/video/upload/v1615082201/video_d2xfyb.mp4";

const Content = (props) => {
    return (
        <Fragment>
            {props.needsHero ? props.setIsHeroBeingUsed(true) : props.setIsHeroBeingUsed(false)}
            {props.needsHero && <Hero source={heroVideo} />}
            <section className={`${props.needsHero ? "main-content md:main-content-md absolute" : "relative"} content-center text-black w-full`}>
                <div className="relative mx-auto container md:px-5 px-2">
                    {props.children}
                </div>
            </section>
        </Fragment>
    )
}

export default Content;