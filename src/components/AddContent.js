import { useState } from "react"
import endpoints from "../endpoints.js";
import SpinnerSVG from "./svg/SpinnerSVG.js";
import AdminSubmitButton from "./utilities/AdminSubmitButton.js";
import DataSelector from "./utilities/DataSelector.js";
import inputStyling from './utilities/inputStyling.js';

const AddAbout = props => {

  const [title, setTitle] = useState(null);
  const [summary, setSummary] = useState(null);
  const [content, setContent] = useState(null);
  const [image, setImage] = useState(null);

  const [titleError, setTitleError] = useState(null);
  const [summaryError, setSummaryError] = useState(null);
  const [contentError, setContentError] = useState(null);
  const [imageError, setImageError] = useState(null);

  const [isLoading, setIsLoading] = useState(false);
  const [message, setMessage] = useState(null);

  const [typeContent, setTypeContent] = useState('gaea_about');

  const clearAll = () => {
    setTitle('');
    setSummary('');
    setContent('');
    setImage('');
  }

  const validate = () => {
    setTitleError(null);
    setSummaryError(null);
    setContentError(null);
    setImageError(null);

    if (!title || title.trim().length === 0) {
      setTitleError("title cannot be empty")
      return false;
    } else if (title.length > 100) {
      setTitleError("title field cannot be longer than 100 characters");
      return false;
    }

    if (!summary || summary.trim().length === 0) {
      setSummaryError("summary field cannot be empty");
      return false;
    } else if (summary.length > 300) {
      setSummaryError("summary field cannot be longer than 300 characters");
      return false;
    }

    if (!content || content.trim().length === 0) {
      setContentError("content field cannot be empty");
      return false;
    } else if (summary.length > 1000) {
      setContentError("content field cannot be longer than 1000 characters")
      return false;
    }

    if (image && image.length > 350) {
      setImageError("image URL length cannot be longer than 350 characters");
      return false;
    }

    return true;
  }

  const onSubmit = () => {
    setIsLoading(true);

    if (!validate()) {
      setIsLoading(false);
      return;
    }

    console.log(JSON.stringify([title, summary, content, image]));

    fetch(endpoints.addContent, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        typeContent,
        title,
        summary,
        content,
        'image': (!image || image.trim().length === 0 ? null : image),
        'username': props.loggedInUser
      })
    })
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoading(false);
          clearAll();
          setMessage(result.message);
        }
      )
      .catch(
        (error) => {
          console.log(error);
          setIsLoading(false);
        }
      )

  }

  return (
    <div className="p-1 bg-green-200 border-2 border-green-500 rounded">
      <form>
        <DataSelector label="Content to add" onChange={setTypeContent} value={typeContent}>
          <option value='gaea_about'>About</option>
          <option value='gaea_landing'>Landing</option>
        </DataSelector>
      </form>
      <form onSubmit={(e) => {
        e.preventDefault();
        onSubmit();
      }}>

        <div className="flex flex-col space-y-4 w-full">
          <input className={inputStyling} type="text" onChange={(e) => setTitle(e.target.value)} value={title} name="title" placeholder="title" />
          {titleError && <div className="font-semibold text-rose-900">Error: {titleError}</div>}
          <input className={inputStyling} type="text" onChange={(e) => setSummary(e.target.value)} value={summary} name="summary" placeholder="summary" />
          {summaryError && <div className="font-semibold text-rose-900">Error: {summaryError}</div>}
          <textarea className={inputStyling} type="text" onChange={(e) => setContent(e.target.value)} value={content} name="content" placeholder="content" />
          {contentError && <div className="font-semibold text-rose-900">Error: {contentError}</div>}
          <input className={inputStyling} type="text" onChange={(e) => setImage(e.target.value)} value={image} name="image" placeholder="image URL" />
          {imageError && <div className="font-semibold text-rose-900">Error: {imageError}</div>}
        </div>

        <div className="flex w-full justify-end py-2 items-center">
          {message &&
            <div className="font-semibold text-green-500">{message}</div>
          }
          {isLoading &&
            <div className="">
              <SpinnerSVG />
            </div>
          }
          <button className="bg-green-400 font-bold px-2 p-1 transition-all duration-700 mx-2 rounded-sm hover:bg-cyan-400" type="button" onClick={() => clearAll()}>Clear all</button>
          <AdminSubmitButton title="Submit" />
        </div>

      </form>
    </div>
  )
}

export default AddAbout;