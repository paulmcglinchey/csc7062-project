import ChevronSVG from '../components/svg/ChevronSVG.js';
import { Link } from 'react-router-dom';


const AboutCard = (props) => {

    return (
        <Link to={`/aboutArticle/${props.articleID}`} className="group">
            <div style={{ backgroundImage: `url("${props.url}")` }} className="h-96 bg-black rounded-lg transition-all duration-500 shadow-2xl bg-cover bg-center transform hover:scale-105">
                <div className="flex flex-col p-2 h-full space-y-2">
                    <div className="flex-grow">
                        <h1 className="inline-block text-4xl font-bold text-springgreen mb-5">{props.title}</h1>
                    </div>
                    <div className="flex self-end order-last">
                        <p className="text-white self-end tracking-wide text-2xl font-medium decoration-slice bg-black inline p-1">{props.summary}</p>
                    </div>
                    <div className="flex self-end justify-end items-center border-2 rounded-full p-2 mt-3 group-hover:border-black group-hover:text-springgreen group-hover:bg-black transition-all duration-300 invisible group-hover:visible">
                        <span>View</span>
                        <span className="ml-2">
                            <ChevronSVG width="w-5" height="h-5" />
                        </span>
                    </div>
                </div>
            </div >
        </Link>
    )
}

export default AboutCard;