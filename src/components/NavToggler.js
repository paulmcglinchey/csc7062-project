import { Fragment } from "react"

const NavToggler = (props) => {

    return (
        <Fragment>
            <button className="toggler-holder focus:outline-none order-2 md:order-last h-16 md:h-12 z-30" onClick={() => props.toggler()}>
                <svg viewBox="0 0 10 10" className={`md:nav-toggler-md nav-toggler ${props.isOpen ? "active md:active-md" : ""} h-10 w-10 md:h-8 md:w-8`}>
                    <path d="M1,2 L9,2 M1,5 L9,5 M1,8 L9,8" className="stroke-current"/>
                </svg>
            </button>
        </Fragment>
    )
}

export default NavToggler;