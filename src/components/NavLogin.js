import { Fragment } from "react";
import HeadshotSVG from './svg/HeadshotSVG.js';
import { Link } from 'react-router-dom';

const NavLogin = (props) => {

    return (
        <Fragment>
            {!props.isUserAuthenticated ?
                <button className="toggler-holder focus:outline-none order-2 md:order-last h-16 md:h-12 z-30 mx-2 group" onClick={() => props.toggleLoginModal()}>
                    <HeadshotSVG />
                </button> :
                <button className="toggler-holder focus:outline-none order-2 md:order-last h-16 md:h-12 z-30 mx-2 group">
                    <Link to="/admin" >
                        <HeadshotSVG />
                    </Link>
                </button>
            }
        </Fragment>
    )
}

export default NavLogin;