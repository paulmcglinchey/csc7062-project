import Content from './Content.js';
import AboutCard from './AboutCard.js';
import { useEffect, useState } from 'react';
import endpoints from '../endpoints.js';
import SpinnerSVG from './svg/SpinnerSVG.js';

const About = (props) => {

    const [items, setItems] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {

        setIsLoading(true);

        fetch((`${endpoints.getContent}?needImages=1&typeContent=gaea_about`), {
            method: 'GET'
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    setItems(result.data);
                    setIsLoading(false);
                }
            )
            .catch(
                (error) => {
                    console.log(error);
                    setIsLoading(false);
                }
            )
    }, [])

    return (
        <Content needsHero={true} setIsHeroBeingUsed={props.setIsHeroBeingUsed}>
            {isLoading &&
                <div className="flex justify-center items-center">
                    <div className="w-96 h-96">
                        <SpinnerSVG />
                    </div>
                </div>
            }
            {items &&
                <div className="container mx-auto mt-5">
                    <div className="grid auto-cols-max grid-cols-1 xl:grid-cols-2 gap-4 my-4 px-2">
                        {items.map((item, key) => {
                            return (
                                <AboutCard title={item.title} summary={item.summary} articleID={item.id} key={key} url={item.url} />
                            )
                        })}
                    </div>
                </div>
            }
        </Content>
    )
}

export default About;