const DashboardContainer = (props) => {
  return (
    <div className="my-2 border-2 border-gray-400 bg-gray-300 rounded p-2">
      <div>
        <h3 className="text-xl font-bold mb-2 md:mb-5">{props.title}</h3>
      </div>
      <div className="w-full rounded">
        {props.children}
      </div>
    </div>
  )
}

export default DashboardContainer;