import { useEffect, useState } from "react";
import endpoints from "../endpoints";
import SpinnerSVG from "./svg/SpinnerSVG";
import ConfirmationButton from "./utilities/ConfirmationButton";
import DataSelector from "./utilities/DataSelector";

const RemoveContent = props => {

  const [items, setItems] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [message, setMessage] = useState(null);

  const [typeContent, setTypeContent] = useState('gaea_about');

  const deleteItem = (ID) => {
    fetch(`${endpoints.deleteArticle}?articleID=${ID}&username=${props.loggedInUser}&typeContent=${typeContent}`, {
      method: 'DELETE'
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          setMessage(result.message);
        }
      )
      .catch(
        (error) => {
          console.log(error);
        }
      )
  }

  useEffect(() => {

    setIsLoading(true);

    fetch(`${endpoints.getContent}?needImages=0&typeContent=${typeContent}`, {
      method: 'GET'
    })
      .then(res => res.json())
      .then(
        (result => {
          setItems(result.data);
          setIsLoading(false);
        })
      )
      .catch(
        (error) => {
          console.log(error);
          setIsLoading(false);
        }
      )
  }, [message, typeContent])

  return (
    <div className="relative">
      {isLoading &&
        <div className="w-24 h-24 absolute inset-1/2 transform -translate-x-1/2 -translate-y-1/2">
          <SpinnerSVG />
        </div>
      }

      <form>
        <DataSelector label="Entries to view" onChange={setTypeContent} value={typeContent}>
          <option value="gaea_about">About</option>
          <option value="gaea_landing">Landing</option>
        </DataSelector>
      </form>

      {items &&
        <div className="flex flex-col w-full p-2 space-y-2">
          {items.map((item, key) => {
            return (
              <div className="flex justify-between items-center font-bold space-x-2" key={key}>
                <div className="flex-shrink rounded-lg bg-green-400 px-2 py-1">{item.id}</div>
                <div className="flex-grow rounded-lg bg-white shadow-2xl px-2 py-1">{item.title}</div>
                <div>
                  <ConfirmationButton ID={item.id} delete={() => deleteItem(item.id)} />
                </div>
              </div>
            )
          })}
        </div>
      }
    </div>
  )
}

export default RemoveContent;