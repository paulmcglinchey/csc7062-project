import { Fragment } from 'react';
import { NavItem } from './NavItem.js';

const NavCollapser = (props) => {

    return (
        <Fragment>
            {props.isOpen &&
                <ul className={`nav flex flex-col md:flex-row w-full md:w-auto mt-5 md:mt-0 md:mr-3 order-last md:order-2 z-30`}>
                    <NavItem title="GitLab" link="https://gitlab.com/paulmcglinchey/csc7062-project" isRoute={false} toggler={props.toggler} />
                    <NavItem title="Analytics" link="/analytics" isRoute={true} toggler={props.toggler} />
                    <NavItem title="About" link="/about" isRoute={true} toggler={props.toggler} />
                </ul>
            }
        </Fragment >
    )
}

export default NavCollapser;