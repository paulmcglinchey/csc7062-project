import { Fragment, useState } from 'react';
import OptionButton from './utilities/OptionButton.js';

const DataEntryOption = (props) => {

  const [isExpanded, setIsExpanded] = useState(false);
  const toggleExpansion = () => setIsExpanded(!isExpanded);

  return (
    <Fragment>
      <div className="bg-green-200 rounded p-1 hover:bg-emerald-400 border-2 border-green-500 mb-3 transition-all duration-600 subpixel-antialiased">
        <div onClick={() => toggleExpansion()} >
          <div className="flex flex-col justify-between items-center space-y-2 md:space-y-0">
            <div className="w-full">
              <h3 className="text-lg font-bold bg-green-400 rounded px-2">{props.title}</h3>
            </div>
            <div className="flex flex-wrap justify-start w-full">
              {props.data.map((detail, key) => {
                return (
                  <OptionButton text={detail.title} type={detail.type} key={key} />
                )
              })}
            </div>
          </div>
        </div>
        <div name="collapser" className={`bg-green-100 px-2 rounded ${isExpanded ? "h-full mt-2 py-2" : "h-0 mt-0 py-0"} overflow-hidden`}>
          {props.children}
        </div>
      </div>
    </Fragment>
  )

}

export default DataEntryOption;