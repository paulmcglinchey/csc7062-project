const InputError = (props) => {
  return (
    <div className="font-bold text-red-600 border-2 border-red-600 p-1 rounded">
      {props.error}
    </div>
  )
}

export default InputError;