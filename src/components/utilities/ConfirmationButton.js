import { useState } from "react"
import TickMarkSVG from "../svg/TickMarkSVG";
import QuitSVG from "../svg/QuitSVG";

const ConfirmationButton = (props) => {

  const [requested, setRequested] = useState(null);
  const [confirmed, setConfirmed] = useState(null);

  const handleRequest = () => {
    setRequested(true);
  }

  const handleRejection = () => {
    setRequested(false);
  }

  const handleConfirmation = () => {
    setConfirmed(true);
    props.delete(props.ID);
  }

  return (
    <div>
      {!requested && !confirmed &&
        <div className="flex items-center w-12 justify-center mx-2">
          <button className="text-red-500 h-6 w-6 transform hover:scale-110 transition-all duration-300 ease-in-out" onClick={() => handleRequest()} type="button">
            <QuitSVG />
          </button>
        </div>
      }
      {requested && !confirmed &&
        <div className="flex items-center w-12 justify-between space-x-2 mx-2">
          <button className="text-green-500 h-6 w-6 transform hover:scale-110 transition-all duration-300 ease-in-out" type="button" onClick={() => handleConfirmation()}>
            <TickMarkSVG />
          </button>
          <button className="text-red-500 h-6 w-6 transform hover:scale-110 transition-all duration-300 ease-in-out" type="button" onClick={() => handleRejection()}>
            <QuitSVG />
          </button>
        </div>
      }
    </div >
  )
}

export default ConfirmationButton;