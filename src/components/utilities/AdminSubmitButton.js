const AdminSubmitButton = (props) => {
  return (
    <button className="p-1 px-2 my-2 bg-green-400 hover:bg-cyan-400 transition-all duration-600 font-bold rounded-sm" type="submit">
      {props.title}
    </button>
  )
}

export default AdminSubmitButton;