const DataSelector = props => {

  return (
    <div className="flex p-1 justify-between rounded bg-emerald-400 my-2">
      <label className="font-semibold">{props.label}</label>
      <select className="rounded bg-green-100" onChange={(e) => props.onChange(e.target.value)} value={props.value}>
        {props.children}
      </select>
    </div>
  )
}

export default DataSelector;