import { Link } from 'react-router-dom';

export const NavItem = (props) => {

    const getLinkType = () => {
        if (props.isRoute) {
            return (
                <Link className="subpixel-antialiased" to={props.link} onClick={() => props.toggler()}>{props.title}</Link>
            )
        } else {
            return (
                <a className="subpixel-antialiased" href={props.link}>{props.title}</a>
            )
        }
    }

    return (
        <div className="relative z-30 group">
            <li className="px-0 md:px-4 py-1 mb-3 md:mb-0 font-bold text-xl uppercase md:normal-case group-hover:text-springgreen">
                {getLinkType()}
            </li>
        </div>
    )
}

export default NavItem;